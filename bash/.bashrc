#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return
# Enable colors in ls
alias ls='ls --color=auto'
# Simple prompt
PS1='[\u@\h \W]\$ '
# Start fish
exec fish
# added by travis gem
[ -f /home/tmac/.travis/travis.sh ] && source /home/tmac/.travis/travis.sh
