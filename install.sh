#!/bin/env bash
echo 'Cloning script repo to provide libs...'
git clone https://T_Mac@bitbucket.org/T_Mac/scripts.git scripts/.bin
echo 'Done'

#source statusCode for install
. scripts/.bin/lib.statusCode
. scripts/.bin/lib.init-d

# Either removes as regular file or unlinks a symlink and
# saftely handles a hard link so it doesn't delete the link file/dir'
safe_remove () {
	file="$1"
	if [ -L "$file" ] || [ "$(stat -c %h -- "$file")" -gt 1 ]; then
	    unlink $file
	else
		rm -f $file
	fi
}

step "Creating backups"
try mkdir -p "$HOME/backup"
try cp -u --backup=existing --suffix=.backup ~/.bashrc ./backup
try cp -u --backup=existing --suffix=.backup ~/.xinitrc ./backup
try cp -u --backup=existing --suffix=.backup ~/.Xresources ./backup
try cp -u --backup=existing --suffix=.backup ~/.config/qtile/config.py ./backup
try cp -u --backup=existing --suffix=.backup ~/.config/fish/config.fish ./backup
try cp -u --backup=existing --suffix=.backup ~/.config/fish/functions/fish_prompt.fish ./backup
try cp -u --backup=existing --suffix=.backup ~/.vimrc ./backup
try cp -u --backup=existing --suffix=.backup ~/.config/Code/User/settings.json ./backup
next

step "Removing old files:"
try safe_remove ~/.bashrc
try safe_remove ~/.xinitrc
try safe_remove ~/.Xresources
try safe_remove ~/.config/qtile/config.py
try safe_remove ~/.config/fish/config.fish
try safe_remove ~/.config/fish/functions
try safe_remove ~/.vimrc
try safe_remove ~/.config/Code/User/settings.json
next

step "Installing X config:"
try stow xorg
next

step "Installing Qtile config:"
try stow qtile
next

step "Installing fish config:"
try stow fish
next

step "Installing .bashrc"
try stow bash
next

step "Installing git commit template"
try git config --global commit.template $PWD/.git-commit-template.txt
next

step "Installing .vimrc"
try stow vim
next

step "Installing Visual Studio Code config"
try stow vscode
next
