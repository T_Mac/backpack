#
# Place environment variables here
#

# Set some standard vars
set -x VISUAL nano
set -x EDITOR $VISUAL
set -x SHELL /usr/bin/fish

# Shortcuts to code directories
set -x CODE $HOME/code
set -x PRIVATE $CODE/private
set -x PUBLIC $CODE/public

# Setup Go environment
set -x GOPATH $HOME/proj/go
set -x PATH $PATH $GOPATH/bin

# User scripts in ~/.bin
set -x PATH $PATH $HOME/.bin

# Locally compiled binaries in ~/.local/bin
set -x PATH $PATH $HOME/.local/bin

# localy install ruby gems
set -x PATH $PATH /home/tmac/.gem/ruby/2.5.0/bin
set -x LD_LIBRARY_PATH /usr/local/lib $LD_LIBRARY_PATH

set -x SSLKEYLOGFILE $HOME/scratch/chrome_ssl_debug.log
