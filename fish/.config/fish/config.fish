#!/usr/bin/fish

set fish_greeting # Disable shell greeting

status --is-interactive; and source (pyenv init -|psub)

set -l config_dir (dirname (status --current-filename))

# Load environment variables
source $config_dir/vars.fish
source $config_dir/piplocal.fish

# If this is an interactive shell load the users ssh keys
if status --is-interactive
	if command -v keychain
		eval ( findKeys.py -pem | xargs keychain --eval --nogui --ignore-missing )
	else
		echo -e "  \e[36mAutomatic key management: \e[39m[\e[31mDISABLED\e[39m]"
	end
end

function activate-virt
   source venv/bin/activate.fish
end

source_env 
cd $HOME
