#!/usr/bin/fish

function create_env
	if not [ -f ~/.local/pip/pip.lock ]
		mkdir -p ~/.local/pip/venv
		touch ~/.local/pip/pip.lock
		python3 -m venv ~/.local/pip/venv
	end
end

function source_env
	create_env
	# source ~/.local/pip/venv/bin/activate.fish
end