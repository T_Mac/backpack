"" Enable pathogen
execute pathogen#infect()
syntax on
filetype plugin indent on

" ----- Core Settings -----
set nocompatible
set encoding=utf-8
colorscheme srcery
set updatetime=250
set mouse=a

" Set dirctories to store all vim created files eg undofiles, backups, swapfiles
set directory=~/.vim/swap-files//
set undodir=~/.vim/undo-files//
set backupdir=~/.vim/backup-files//

" Enable Undo file creation
set undofile

" Enable relative line numbers
set relativenumber

" Auto indent
set autoindent

" Hard Tabs 4 spaces
set tabstop=4
set softtabstop=4
set shiftwidth=4
set noexpandtab

" Draw a line at 85 characters
"set colorcolumn=85

" Set characters for invisible text
set hidden
" set list
" set listchars=tab:▸\ ,

" Save on loss of focus
au FocusLost * :wa

" ----- Plugin Config -----
" Set git gutter to always be open
let g:gitgutter_sign_column_always = 1
" Set lightline theme
let g:lightline = {
      \ 'colorscheme': 'wombat',
      \ }
" space after comment delim NerdCommenter
let g:NERDSpaceDelims = 1
" File extenstion to ignore in command-t
let g:CommandTWildIgnore=&wildignore . ",*.db"
" Blindly copied need investigation
set scrolloff=3
set noshowmode
set showcmd
set wildmenu
set wildmode=list:longest
set visualbell
set cursorline
set ttyfast
set ruler
set backspace=indent,eol,start
set laststatus=2

" Enable python syntax highlighting
let python_highlight_all = 1

" Search
" Tame case
" all lowercase disables case sensitive
" but even ONE cap enables it
set ignorecase
set smartcase

" Search and replace globally on line by default
set gdefault

" Highlight search terms, as you type
set incsearch
set showmatch
set hlsearch

" Easy clear search
nnoremap <leader><space> :noh<cr>


" Remaps
" Leader Key
let mapleader = ","
" Easy ~/.vimrc source motion
map <leader>s :source ~/.vimrc<CR>
" Disable arrow keys
nnoremap <up> <nop>
nnoremap <down> <nop>
nnoremap <left> <nop>
nnoremap <right> <nop>
inoremap <up> <nop>
inoremap <down> <nop>
inoremap <left> <nop>
inoremap <right> <nop>

" Move by screen line not file line
nnoremap j gj
nnoremap k gk

" Disable Help key
inoremap <F1> <ESC>
nnoremap <F1> <ESC>
vnoremap <F1> <ESC>

" Make tab key match bracket pairs
nnoremap <tab> %
vnoremap <tab> %

nnoremap ; :
" File Tree
nnoremap <leader>nt :NERDTree<cr>
