﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-

from libqtile import bar, hook, layout, widget
from libqtile.command import lazy
from libqtile.config import Drag, Click, Group, Key, Match, Screen
from itertools import chain

# Give the colors some names
class Color(object):
	Parchment = '#EEEEEE'
	Seaweed = '#66BB6A'
	Kush = '#A5D6A7'
	Sherbet = '#FFCC80'
	Ocean = '#81D4FA'
	Strawberry = '#EF9A9A'
	Carbon = '#1D2326'
	Graphite = '#2B2B2B'
	Ash = '#0D0D0D'
	Charcoal = '#595857'
	Plaster = '#D9CFC7'
	Hydro = 'A1CD73'

#  Default style options
class StyleDefaults(object):
	BackgroundColor = Color.Ash
	BackgroundAccentColor = Color.Kush
	PrimaryColor = Color.Seaweed
	AccentColor = Color.Kush
	Font = 'Contrail One'
	FontSize = 18
	FontColor = Color.Charcoal
	FontAccentColor = Color.Graphite
	BarHeight = 30

# Commands to spawn
class Commands(object):
	browser = 'firefox'
	editor = 'code'
	lockscreen = 'slock'
	terminal = 'urxvtc'
	trackpad_toggle = "synclient TouchpadOff(synclient -l | grep -c 'TouchpadOff.*=.*0')"
	volume_up = 'amixer -q -c 1 sset Master 5dB+'
	volume_down = 'amixer -q -c 1 sset Master 5dB-'
	volume_toggle = 'amixer -q -D pulse sset Master 1+ toggle'
	brightness_up = 'light -A 5'
	brightness_down = 'light -U 5'
	kbd_brightness_up = 'kbdbright -u'
	kbd_brightness_down = 'kbdbright -d'
	launcher = 'ulauncher'


class GraphConfig:
# Container for graph specific config (because there's a lot)
	base = dict(
		background=StyleDefaults.BackgroundColor,
		border_width=0,
		# border_color=StyleDefaults.PrimaryColor,
		line_width=1,
		margin_x=5,
		margin_y=5,
		width=175,
		frequency=.5,
	)

	CPU = dict(
		graph_color = Color.Strawberry,
		fill_color = Color.Strawberry
	)

	MEM = dict(
		graph_color = Color.Ocean,
		fill_color = Color.Ocean
	)

	ETHERNET1 = dict(
		interface = 'eno1',
		graph_color = Color.Sherbet,
		fill_color = Color.Sherbet
	)

	ETHERNET0 = dict(
		interface = 'enp5s0',
		graph_color = Color.Seaweed,
		fill_color = Color.Seaweed
	)



# active/inactive - font color
# *_screen_border - Inactive border
# *_current_screen_border - Active border, Font color in text mode

class Widget(object):
	defaults = dict(
		fontsize = StyleDefaults.FontSize,
		foreground = StyleDefaults.FontColor
	)
# Container for individual widget style options
	groupbox = dict(
		active=StyleDefaults.FontColor,
		inactive=StyleDefaults.FontAccentColor,
		# this_screen_border=StyleDefaults.PrimaryColor,
		this_current_screen_border=StyleDefaults.AccentColor,#'#e8f5e9',
		# other_screen_border='#e0e0e0',
		# other_current_screen_border=StyleDefaults.AccentColor,

		highlight_method='text',
		rounded=False,
		# foregroud=Color.Kush,
		margin=0,
		padding=1,
		spacing = 12,
		borderwidth=1,
		disable_drag=True,
		invert_mouse_wheel=True,
		fontsize= StyleDefaults.FontSize + 4
	)

	sep = dict(
		foreground=StyleDefaults.BackgroundAccentColor,
		height_percent=75,
		padding=10,
	)

	battery_text = dict(
		charge_char='',  # fa-arrow-up
		discharge_char='',  # fa-arrow-down
		format='{char} {percent:2.0%} {hour:d}:{min:02d}',
		hide_threshold=98,
	)

	battery = dict (
		theme_path = '/home/tmac/.backpack/qtile'
	)

	weather = dict(
		update_interval=60,
		metric=False,
		format='{condition_text} {condition_temp}°',
	)
screens = [Screen(
			top=bar.Bar(widgets=[
			widget.Spacer(length=10),
			widget.GroupBox(**Widget.groupbox),
			widget.Spacer(length=5),
			widget.Sep(**Widget.sep),
			widget.Spacer(length=5),
			widget.CurrentLayout(padding = 5, foreground=StyleDefaults.FontColor, fontsize= StyleDefaults.FontSize + 2),
			widget.Sep(**Widget.sep),
			widget.WindowName(width = bar.CALCULATED, **Widget.defaults),
			widget.Spacer(length=10),
			# widget.Prompt(fontsize = StyleDefaults.FontSize, foreground = StyleDefaults.AccentColor, prompt = '> '),

			#  widget.DebugInfo(),

			widget.Spacer(),
			widget.CPUGraph(**GraphConfig.CPU, **GraphConfig.base),
			widget.MemoryGraph(**GraphConfig.MEM, **GraphConfig.base),
			widget.NetGraph(**GraphConfig.ETHERNET0, **GraphConfig.base),
			# widget.NetGraph(**GraphConfig.ETHERNET1,  **GraphConfig.base),
			widget.Spacer(length=2),
			widget.Systray(),
			widget.Spacer(length=2),
			# widget.Battery(**Widget.battery_text),
			widget.Clock(format='%a %b %d, %I:%M %P', foreground=StyleDefaults.AccentColor, fontsize = StyleDefaults.FontSize),
			], size = StyleDefaults.BarHeight, background = StyleDefaults.BackgroundColor),
		) for _ in range(2)]

# Keybindings

# This setup uses 3 modifier keys
# meta - the main key for shortcuts,
#         everything, more or less, uses this key
# mod   - secondary modifier key, think of it as shift
# alt   - for when super needs to do something else

meta = 'mod3'
mod = 'shift'
alt = 'mod1' # alt

keys = [
	# Window Manager Controls
	Key([meta, alt], 'q', lazy.shutdown()), # for when shit hits the fan
	Key([meta, alt], 'r', lazy.restart()), # reload your config

	# Launcher
	Key([meta], 'r', lazy.spawn(Commands.launcher)),

	# Window Controls
	Key([meta], 'k', lazy.window.kill()),
	Key([meta], 'f', lazy.window.toggle_floating()),
	Key([meta, mod], 'j', lazy.layout.shuffle_up()),
	Key([meta, mod], 'k', lazy.layout.shuffle_down()),
	Key([meta, mod], 'g', lazy.layout.grow()),
	Key([meta, mod], 's', lazy.layout.shrink()),
	Key([meta, mod], 'n', lazy.layout.normalize()),
	Key([meta, mod], 'm', lazy.layout.maximize()),

	# Switch groups
	Key([meta], 'Left', lazy.screen.prev_group()),
	Key([meta], 'Right', lazy.screen.next_group()),

	# Cycle layouts
	Key([meta], 'Up', lazy.next_layout()),
	Key([meta], 'Down', lazy.prev_layout()),

	# Change window focus
	Key([meta], 'space', lazy.layout.next()),
	Key([meta, mod], 'space', lazy.layout.previous()),

	# Spawn Terminal
	Key([meta], 'Return', lazy.spawn(Commands.terminal)),

	#Brightness Controls
	Key([], 'XF86MonBrightnessUp', lazy.spawn(Commands.brightness_up)),
	Key([], 'XF86MonBrightnessDown', lazy.spawn(Commands.brightness_down)),

	Key([], 'XF86KbdBrightnessUp', lazy.spawn(Commands.kbd_brightness_up)),
	Key([], 'XF86KbdBrightnessDown', lazy.spawn(Commands.kbd_brightness_down)),
	# Commands: Volume Controls
	Key([], 'XF86AudioRaiseVolume', lazy.spawn(Commands.volume_up)),
	Key([], 'XF86AudioLowerVolume', lazy.spawn(Commands.volume_down)),
	Key([], 'XF86AudioMute', lazy.spawn(Commands.volume_toggle)),

	# Touchpad toggle
	Key([], 'XF86TouchpadToggle', lazy.spawn(Commands.trackpad_toggle)),

	#  Lockscreen
	Key([meta, 'control'], 'l', lazy.spawn(Commands.lockscreen)),

]


def app_or_group(group, app, createGroup = False, jumpToGroup = False):
	""" Go to specified group if it exists. Otherwise, run the specified app.
	When used in conjunction with dgroups to auto-assign apps to specific
	groups, this can be used as a way to go to an app if it is already
	running. pass createGroup, to create it if it doesn't exist"""
	def f(qtile):
		try:
			 qtile.groupMap[group].cmd_toscreen()
		except KeyError:
			qtile.cmd_spawn(app)
	return f


# wm_class matches second item in WM_CLASS
# wm_instance_class mathes first

# matches = {
# 	'wm_class' 'MyWM_CLASS',
# 	'wm_instance_class' : 'MyWM_INSTANCE_CLASS'
# }

# Hotkeys
# False   - No hotkey will be assigned
# True    - Try to auto-assign a hotkey starting with first letter of name
# '<key>' - Assign hotkey to <key>


# Panes hang around and are intended for long running apps
class Pane:
	_type = 'pane'
	name = None			# override use __class__.__name__
	hotkey = None		# hotkeys are optional
	layout = 'max'		# default to fullscreen
	matches = {}		# which windows should go here?
	init = True			# Panes spawn at startup
	exclusive = False	# You can spawn any proc in your Pane
	position = None		# None invokes implict ordering
	spawn = None		# should we start any procs?
	persists = True     # Should we stay alive after all procs have died?


# Apps come and go as they run and die
# hotkeys are automatic for apps
class App(Pane):
	_type = 'app'
	init = False
	exclusive = True
	persists = False
	hotkey = True
	app = None

def _subclsr(parent, **kwargs):
	class blank(parent):
		pass
	for k, v in kwargs.items():
		setattr(blank, k, v)
	return blank

def matcher(**kwargs):
	return {k:v for k, v in kwargs.items() if v is not None}

def pane(title, wm_class = None, wm_instance_class = None, hotkey = None):
	matches = matcher(wm_class=wm_class, wm_instance_class=wm_instance_class)
	return _subclsr(Pane, name = title, matches = matches, hotkey = hotkey)

def app(title, wm_class = None, wm_instance_class = None, hotkey = None):
	matches = matcher(wm_class=wm_class, wm_instance_class=wm_instance_class)
	return _subclsr(App, name = title, matches = matches, hotkey = hotkey)

def tab(title, wm_class = None, wm_instance_class = None):
	matches = matcher(wm_class=wm_class, wm_instance_class=wm_instance_class)
	return _subclsr(App, name = title, matches = matches, hotkey = False)

# You can use classes to represent individual panes and apps
class Home(Pane):
	matches = dict(wm_instance_class=['home-panel'])
	hotkey = 'h'
	layout = 'monad'
	spawn = ['urxvtc -e weatherboard', 'urxvtc -name home-panel']

class Dev(Pane):
	matches = dict(wm_instance_class=['dev-panel'])
	hotkey = 'd'
	layout = 'tile'
	spawn = ['urxvtc -name dev-panel']

class Editor(App):
	matches = dict(wm_class=['Code'])
	hotkey = 'e'
	app = [Commands.editor]

class Web(App):
	matches = dict(wm_class=['Vivaldi-stable', 'Firefox'])
	hotkey = 'c'
	app = [Commands.browser]

class Kicad(App):
	matches = dict(wm_class=['Kicad'])
	hotkey = 'i'
	app = ['kicad']

class Eagle(App):
	matches = dict(wm_class=['Eagle'])
	hotkey = 'p'
	app = ['eagle']

# Or you can use the convience functions for the easy ones
# Here we're creating some named apps, with no hotkeys that comes
# and goes with the application

# app define a supplementary to add at the end of workspaces
apps = [
	tab('Screeps', ['Screeps']),
	tab('Dwarf Fortress', ['Dwarf_Fortress']),
	tab('Telegram', ['TelegramDesktop']),
	tab('Inkscape', ['inkscape']),
]

# workspaces defines a list of groups to add, ordering is preserved
workspaces = [
	Home,
	Dev,
	Editor,
	Web,
	Kicad,
	Eagle,
	pane('Logs', hotkey = True),
	pane('1', hotkey = True),
	pane('2', hotkey = True),
	pane('3', hotkey = True),
	pane('4', hotkey = True)
]

# modifier keys to use for groups
# <meta> + hotkey to move group to screen/launch app
# <alt> + <meta> + hotkey to move active window to group

groups = []
used_hotkeys = []

# Position is determined by the order of workspaces
position = 0
for ws in chain(workspaces, apps):
	title = ws.name if ws.name is not None else ws.__name__
	hotkey = ws.hotkey
	if hotkey is True: # Try to find a letter of the title thats not already taken
		available_hotkeys = list(filter(lambda h: h not in used_hotkeys, title.lower()))
		if available_hotkeys: # Choose the first one
			hotkey = available_hotkeys[0]
		else:  # Couldn't find any :(
			hotkey = False
	if hotkey:
		used_hotkeys.append(hotkey)

	func = None
	if ws._type == 'app':
		if hotkey: # If we've got a hotkey
			if ws.app: # And an attached proc to launch
				func = lazy.function(app_or_group(title, ws.app))
			else: # If no attached app
				func = lazy.group[title].toscreen()
	elif ws._type == 'pane':
		if hotkey: # Panes have no attached apps so we just switch to it
			func = lazy.group[title].toscreen()

	if hotkey:
		keys.append(Key([meta], hotkey, func))
		keys.append(Key([meta, alt], hotkey, lazy.window.togroup(title)))

	matches = [Match(**ws.matches)] if ws.matches else None
	opts = dict(
		layout = ws.layout,
		matches = matches,
		init = ws.init,
		exclusive = ws.exclusive,
		position = position,
		spawn = ws.spawn,
		persist = ws.persists)
	print(opts)
	groups.append(Group(title, **opts))
	position += 10

# Allow us to easily move windows
mouse = (
	Drag([meta], 'Button1', lazy.window.set_position_floating(),
		start=lazy.window.get_position()),
	Drag([meta], 'Button3', lazy.window.set_size_floating(),
		start=lazy.window.get_size()),
)

#
bring_front_click = True


layout_defaults = dict(
	border_width=1,
	margin=0,
	border_focus=StyleDefaults.BackgroundAccentColor,
	border_normal=StyleDefaults.BackgroundColor,
)

layouts = (
	layout.Max(**layout_defaults),
	layout.Tile(ratio=0.5, **layout_defaults),
	layout.RatioTile(**layout_defaults),
	layout.Matrix(**layout_defaults),
	layout.MonadTall(ratio=0.5, name='monad', **layout_defaults),
)

floating_layout = layout.floating.Floating(
	auto_float_types=(
		'notification',
		'toolbar',
		'splash',
		'dialog',
	),
	float_rules=[{ 'wmclass': x, 'wname': y} for x,y in (
		('Ulauncher', 'Ulauncher window title'),
	)],
	**layout_defaults
)

@hook.subscribe.client_new
def floating_dialogs(window):
	dialog = window.window.get_wm_type() == 'dialog'
	transient = window.window.get_wm_transient_for()
	if dialog or transient:
		window.floating = True

eagle_dialogs = (
	'ADD',
	'Properties'
)

@hook.subscribe.client_new
def floating_eagle(window):
	if window.window.get_wm_class() == ('eagle', 'Eagle') and window.window.get_name() in eagle_dialogs:
		window.floating = True
		window.bring_to_front()



@hook.subscribe.addgroup
def swithToGroup(qtile, group):
	qtile.groupMap[group].cmd_toscreen()


#floating and Always on top

# *****************
# *               *
# *    HOTKEYS    *
# *               *
# *****************

# # Window Manager Controls
#  meta, alt q shutdown  
#  meta, alt r restart  

# # Launcher
#  meta r  launcher 

# # Window Controls
#  meta k   kill
#  meta f   toggle_floating
#  meta, mod j   shuffle_up
#  meta, mod k   shuffle_down
#  meta, mod g   grow
#  meta, mod s   shrink
#  meta, mod n   normalize
#  meta, mod m   maximize

# # Switch groups
#  meta Left   prev_group
#  meta Right   next_group

# # Cycle layouts
#  meta Up next_layout  
#  meta Down prev_layout  

# # Change window focus
#  meta space   next
#  meta, mod space   previous

# # Spawn Terminal
#  meta Return  terminal 

# #Brightness Controls
#  XF86MonBrightnessUp  brightness_up 
#  XF86MonBrightnessDown  brightness_down 

#  XF86KbdBrightnessUp  kbd_brightness_up 
#  XF86KbdBrightnessDown  kbd_brightness_down 
# # Commands: Volume Controls
#  XF86AudioRaiseVolume  volume_up 
#  XF86AudioLowerVolume  volume_down 
#  XF86AudioMute  volume_toggle 

# # Touchpad toggle
#  XF86TouchpadToggle  trackpad_toggle 

# #  Lockscreen
#  meta, control l  lockscreen 

